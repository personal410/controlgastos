//
//  Subcategory.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Subcategory:NSManagedObject{
    override var description:String {
        return "<subcategoryId: \(self.subcategoryId), subcategoryName: \(self.subcategoryName)>"
    }
    class func createSubcategory(subcategoryName:String, defaultAmount:Float, category:Category){
        let context = AppDelegate.getApplicationManagedObjectContext()
        let subcategoryId = category.subcategories!.count + 1
        let entity = NSEntityDescription.entity(forEntityName: "Subcategory", in: context)
        let newSubcategory = Subcategory(entity: entity!, insertInto: context)
        newSubcategory.subcategoryId = Int16(subcategoryId)
        newSubcategory.subcategoryName = subcategoryName
        newSubcategory.defaultAmount = defaultAmount
        newSubcategory.category = category
        do{
            try context.save()
        }catch let error as NSError {
            print("error in createCategory: \(error)")
        }
    }
    class func deleteSubcategory(subcategory:Subcategory){
        let context = AppDelegate.getApplicationManagedObjectContext()
        context.delete(subcategory)
        do{
            try context.save()
        }catch let error as NSError {
            print("error in deleteCategory: \(error)")
        }
    }
}
