//
//  Expense.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Expense:NSManagedObject{
    override var description:String {
        return "<expenseId: \(self.expenseId), amount: \(self.amount), subcategory: \(self.subcategory), comment: \(self.comment)>"
    }
    class func createExpense(subcategory:Subcategory, amount:Float, date:NSDate, comment:String) -> Expense? {
        let day = Day.getDayWithDate(date: date)
        var expenseId = 1
        for expense in day.expenses!.array as! [Expense] {
            if Int(expense.expenseId) == expenseId {
                expenseId += 1
            }else if Int(expense.expenseId) > expenseId {
                break
            }
        }
        let context = AppDelegate.getApplicationManagedObjectContext()
        let entity = NSEntityDescription.entity(forEntityName: "Expense", in: context)
        let newExpense = Expense(entity: entity!, insertInto: context)
        newExpense.expenseId = Int32(expenseId)
        newExpense.subcategory = subcategory
        newExpense.amount = amount
        newExpense.day = day
        newExpense.comment = comment
        day.totalAmount = day.totalAmount + Double(amount)
        do{
            try context.save()
            return newExpense
        }catch let error as NSError {
            print("error in createExpense: \(error)")
            return nil
        }
    }
    class func deleteExpense(expense:Expense){
        let context = AppDelegate.getApplicationManagedObjectContext()
        context.delete(expense)
        AppDelegate.saveContext()
    }
}
