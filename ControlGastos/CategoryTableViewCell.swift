//
//  CategoryTableViewCell.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import UIKit
class CategoryTableViewCell:UITableViewCell{
    @IBOutlet weak var categoryNameTxtFld:UITextField!
    @IBOutlet weak var detailBtn:UIButton!
}