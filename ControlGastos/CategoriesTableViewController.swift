//
//  CategoriesTableViewController.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import UIKit
class CategoriesTableViewController:UITableViewController, UITextFieldDelegate{
    var arrCategories = Category.getAllCategories()
    var currentTxtFld:UITextField?
    //MARK: - ViewController
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    override func viewWillDisappear(_ animated:Bool){
        super.viewWillDisappear(animated)
        currentTxtFld?.resignFirstResponder()
        NotificationCenter.default.removeObserver(self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let button = sender as? UIButton {
            let viewCont = segue.destination as! SubcategoriesTableViewController
            viewCont.category = arrCategories[button.tag]
        }
    }
    //MARK: - TableView
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrCategories.count + 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath as IndexPath) as! CategoryTableViewCell
        cell.detailBtn.isHidden = (indexPath.row == arrCategories.count)
        cell.categoryNameTxtFld.tag = indexPath.row
        cell.categoryNameTxtFld.text = ""
        if indexPath.row < arrCategories.count {
            let categoryTemp = arrCategories[indexPath.row]
            cell.categoryNameTxtFld.text = categoryTemp.categoryName
            cell.detailBtn.tag = indexPath.row
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row < arrCategories.count
    }
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Eliminar"
    }
    override func tableView(_ tableView:UITableView, commit editingStyle:UITableViewCell.EditingStyle, forRowAt indexPath:IndexPath){
        if editingStyle == .delete {
            let categoryTemp = arrCategories[indexPath.row]
            var subcategoryHasSomeDays = false
            for subcategory in categoryTemp.subcategories!.array as! [Subcategory] {
                if subcategory.expenses!.allObjects.count > 0 {
                    subcategoryHasSomeDays = true
                    break
                }
            }
            if subcategoryHasSomeDays {
                let alertCont = UIAlertController(title: "Alerta", message: "No se puede eliminar esta categoria", preferredStyle: .alert)
                alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertCont, animated: true, completion: nil)
            }else{
                Category.deleteCategory(category: categoryTemp)
                arrCategories.remove(at: indexPath.row)
                self.tableView.beginUpdates()
                self.tableView.deleteRows(at: [indexPath], with: .top)
                self.tableView.endUpdates()
            }
        }
    }
    //MARK: - TextField
    func textFieldDidBeginEditing(_ textField:UITextField){
        currentTxtFld = textField
    }
    func textFieldDidEndEditing(_ textField:UITextField){
        if textField.tag < arrCategories.count {
            let categoryTemp = arrCategories[textField.tag]
            textField.text = categoryTemp.categoryName
        }
    }
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        let categoryNameTemp = textField.text!
        if !categoryNameTemp.isEmpty {
            if textField.tag < arrCategories.count {
                let categoryTemp = arrCategories[textField.tag]
                let arrCategoriesTemp = arrCategories.filter(){$0.categoryName == categoryNameTemp}
                if arrCategoriesTemp.count > 0 {
                    if categoryTemp != arrCategoriesTemp.first! {
                        let alertCont = UIAlertController(title: "Alerta", message: "No se pueden repetir las categorias", preferredStyle: .alert)
                        alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alertCont, animated: true, completion: nil)
                        return false
                    }
                }else{
                    categoryTemp.categoryName = categoryNameTemp
                    ToolBox.updateContext()
                }
            }else{
                let newCategory = Category.createCategory(categoryName: categoryNameTemp)
                arrCategories.append(newCategory!)
                self.tableView.beginUpdates()
                self.tableView.reloadRows(at: [IndexPath(row: arrCategories.count - 1, section: 0)], with: .none)
                self.tableView.insertRows(at: [IndexPath(row: arrCategories.count, section: 0)], with: .top)
                self.tableView.endUpdates()
            }
        }
        currentTxtFld = nil
        textField.resignFirstResponder()
        return true
    }
    //MARK: - Keyboard
    @objc func showKeyboard(notification:NSNotification){
        let info = notification.userInfo!
        let keyboardFrameValue = info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        let keyboardHeight = keyboardFrame.height
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
    }
    @objc func hideKeyboard(_:NSNotification){
        let newEdgeInset = UIEdgeInsets.zero
        self.tableView.contentInset = newEdgeInset
        self.tableView.scrollIndicatorInsets = newEdgeInset
    }
}
