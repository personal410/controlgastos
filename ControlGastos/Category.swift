//
//  Category.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Category:NSManagedObject{
    override var description:String {
        return "<categoryId: \(self.categoryId), categoryName: \(self.categoryName), subcategories: \(self.subcategories!.array)>"
    }
    class func createCategory(categoryName:String) -> Category? {
        let context = AppDelegate.getApplicationManagedObjectContext()
        let categories = getAllCategories()
        let categoryId = categories.count + 1
        let entity = NSEntityDescription.entity(forEntityName: "Category", in: context)
        let newCategory = Category(entity: entity!, insertInto: context)
        newCategory.categoryId = Int16(categoryId)
        newCategory.categoryName = categoryName
        do{
            try context.save()
            return newCategory
        }catch let error as NSError {
            print("error in createCategory: \(error)")
            return nil
        }
    }
    class func getAllCategories() -> [Category] {
        let context = AppDelegate.getApplicationManagedObjectContext()
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
        do {
            let results = try context.fetch(fetchReq)
            return results as! [Category]
        }catch let error as NSError {
            print("error in getAllCategories: \(error)")
            return []
        }
    }
    class func deleteCategory(category:Category){
        let context = AppDelegate.getApplicationManagedObjectContext()
        context.delete(category)
        do{
            try context.save()
        }catch let error as NSError {
            print("error in deleteCategory: \(error)")
        }
    }
}
