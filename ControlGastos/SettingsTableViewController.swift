//
//  SettingsTableViewController.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import UIKit
class SettingsTableViewController:UITableViewController{
    let arrOptions = [["Categorias"]]
    //MARK: - TableView
    override func numberOfSections(in tableView: UITableView) -> Int {
        return arrOptions.count
    }
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrOptions[section].count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)
        cell.textLabel?.text = arrOptions[indexPath.section][indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: "showCategories", sender: nil)
        }
    }
}
