//
//  Day.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Day:NSManagedObject{
    override var description:String {
        return "<date: \(self.date), totalAmount: \(self.totalAmount), expenses: \(self.expenses!.array)>"
    }
    class func getDayWithDate(date:NSDate) -> Day {
        let days = getAllDaysWithDate(date: date)
        if days.count == 0 {
            let context = AppDelegate.getApplicationManagedObjectContext()
            let entity = NSEntityDescription.entity(forEntityName: "Day", in: context)
            let newDay = Day(entity: entity!, insertInto: context)
            newDay.date = date
            newDay.totalAmount = 0
            try! context.save()
            return newDay
        }else{
            return days.first!
        }
    }
    class func getAllDays() -> [Day] {
        let context = AppDelegate.getApplicationManagedObjectContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Day")
        do{
            let results = try context.fetch(fetchRequest)
            return results as! Array<Day>
        }catch let error as NSError {
            print(error)
            return []
        }
    }
    class func getAllDaysWithDate(date:NSDate? = nil) -> Array<Day> {
        let context = AppDelegate.getApplicationManagedObjectContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Day")
        if date != nil {
            fetchRequest.predicate = NSPredicate(format: "date == %@", date!)
        }
        do{
            let results = try context.fetch(fetchRequest)
            return results as! Array<Day>
        }catch let error as NSError {
            print(error)
            return []
        }
    }
}
