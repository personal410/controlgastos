//
//  SubcategoriesExpensesTableViewController.swift
//  ControlGastos
//
//  Created by Victor Salazar on 26/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import UIKit
class SubcategoriesExpensesTableViewController:UITableViewController{
    //MARK: - Variables
    var arrExpenses:Array<Expense>!
    var arrExpensesBySubcategories:Array<Array<Expense>>!
    var indexPathToModified:NSIndexPath?
    var expenseModified:Expense?
    var newExpense:Expense?
    //MARK: - IBOutlet
    @IBOutlet weak var categoryNameLbl:UILabel!
    //MARK: - ViewController
    override func viewDidLoad(){
        super.viewDidLoad()
        self.separateExpensesBySubcategory()
        self.navigationItem.title = ToolBox.getDate(date: arrExpensesBySubcategories.first!.first!.day.date, withFormat: "dd/MM/yyyy")
        self.categoryNameLbl.text = arrExpensesBySubcategories.first!.first!.subcategory.category.categoryName
    }
    override func viewWillAppear(_ animated:Bool){
        if expenseModified != nil {
            self.arrExpensesBySubcategories[indexPathToModified!.section][indexPathToModified!.row] = self.expenseModified!
            expenseModified = nil
            self.tableView.reloadRows(at: [indexPathToModified! as IndexPath], with: .none)
        }else{
            if newExpense != nil {
                arrExpenses.append(newExpense!)
                self.separateExpensesBySubcategory()
                self.tableView.reloadData()
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewCont = segue.destination as? NewExpenseViewController {
            if let indexPath = sender as? NSIndexPath {
                indexPathToModified = indexPath
                let expense = arrExpensesBySubcategories[indexPath.section][indexPath.row]
                viewCont.expense = expense
            }else{
                viewCont.lockedCategory = self.arrExpenses.first!.subcategory.category
                viewCont.lockedDate = self.arrExpenses.first!.day.date
            }
        }
    }
    // MARK: - TableView
    override func numberOfSections(in tableView: UITableView) -> Int {
        return arrExpensesBySubcategories.count
    }
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrExpensesBySubcategories[section].count
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arrExpensesBySubcategories[section].first?.subcategory.subcategoryName
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "expenseCell", for: indexPath)
        let expense = arrExpensesBySubcategories[indexPath.section][indexPath.row]
        cell.textLabel?.text = !expense.comment.isEmpty ? expense.comment : "Sin comentario"
        cell.detailTextLabel?.text = String(format: "S/. %0.2f", arguments: [expense.amount])
        return cell
    }
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let modifyRowAction = UITableViewRowAction(style: .default, title: "Modificar") { (action, indexPath) in
            self.performSegue(withIdentifier: "updateExpense", sender: indexPath)
        }
        modifyRowAction.backgroundColor = UIColor.lightGray
        let deleteRowAction = UITableViewRowAction(style: .destructive, title: "Eliminar") { (action, indexPath)  in
            let expense = self.arrExpensesBySubcategories[indexPath.section][indexPath.row]
            let day = expense.day
            self.arrExpenses.remove(at: self.arrExpenses.firstIndex(of: expense)!)
            day.totalAmount -= Double(expense.amount)
            AppDelegate.saveContext()
            Expense.deleteExpense(expense: expense)
            if self.arrExpenses.count == 0 {
                self.navigationController?.popToRootViewController(animated: true)
            }else{
                self.separateExpensesBySubcategory()
                self.tableView.reloadData()
            }
        }
        return [deleteRowAction, modifyRowAction]
    }
    //MARK: - IBAction
    @IBAction func createNewExpense(){
        self.performSegue(withIdentifier: "updateExpense", sender: nil)
    }
    //MARK: - Auxiliar
    func separateExpensesBySubcategory(){
        arrExpensesBySubcategories = []
        let categoryTemp = self.arrExpenses.first!.subcategory.category
        for subcategory in categoryTemp.subcategories!.array as! Array<Subcategory> {
            let arrExpensesFilter = arrExpenses.filter(){$0.subcategory.subcategoryName == subcategory.subcategoryName}
            if arrExpensesFilter.count > 0 {
                arrExpensesBySubcategories.append(arrExpensesFilter)
            }
        }
    }
}
