//
//  MonthYear.swift
//  ControlGastos
//
//  Created by victor salazar on 21/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import Foundation
class MonthYear:CustomStringConvertible{
    var month:Int
    var year:Int
    var numberDays:Int
    var firstDay:Int
    var numberLines:Int
    init(date:NSDate){
        let calendar = ToolBox.getCalendar()
        let dateComps = calendar.components([.month, .year], from: date as Date)
        self.month = dateComps.month!
        self.year = dateComps.year!
        self.numberDays = calendar.range(of: .day, in: .month, for: date as Date).length
        let weedayTemp = calendar.component(.weekday, from: date as Date)
        self.firstDay = (weedayTemp + 5) % 7 + 1
        self.numberLines = Int(ceil(Float(self.firstDay + self.numberDays - 1) / 7.0))
    }
    func dateTemp() -> NSDate {
        let dateComps = NSDateComponents()
        dateComps.month = self.month
        dateComps.year = self.year
        dateComps.day = 1
        return ToolBox.getCalendar().date(from: dateComps as DateComponents)! as NSDate
    }
    var description:String {
        return "<month: \(self.month), year: \(self.year), numberDays: \(self.numberDays), firstDay: \(self.firstDay)>"
    }
}
func ==(left:MonthYear, right: MonthYear) -> Bool {
    return (left.month == right.month) && (left.year == right.year)
}
