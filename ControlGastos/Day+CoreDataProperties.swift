//
//  Day+CoreDataProperties.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Day{
    @NSManaged var date:NSDate
    @NSManaged var totalAmount:Double
    @NSManaged var expenses:NSOrderedSet?
}