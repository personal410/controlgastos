//
//  Tools.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import Foundation
import UIKit
class ToolBox{
    class func getCalendar() -> NSCalendar {
        let calendar = NSCalendar(identifier: .gregorian)!
        calendar.locale = Locale(identifier: "en_US")
        return calendar
    }
    class func getDate(date:NSDate, withFormat format:String) -> String {
        let dateFormatter = self.getDateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date as Date)
    }
    class func getDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.calendar = self.getCalendar() as Calendar
        return dateFormatter
    }
    class func updateContext(){
        let context = AppDelegate.getApplicationManagedObjectContext()
        do{
            try context.save()
        }catch let error as NSError {
            print("error in update: \(error)")
        }
    }
    class func normalizeDate(date:NSDate) -> Date {
        let calendar = self.getCalendar()
        var dateComps = calendar.components([.month, .year], from: date as Date)
        dateComps.day = 1
        return calendar.date(from: dateComps)!
    }
    class func getMonthWithDate(date:NSDate, withDirection direction:Int) -> NSDate {
        let dateComps = NSDateComponents()
        dateComps.month = direction
        return ToolBox.getCalendar().date(byAdding: dateComps as DateComponents, to: date as Date, options: .matchFirst)! as NSDate
    }
    class func showAlertWithTitle(title:String, withMessage message:String, inViewCont viewCont:UIViewController){
        let alertCont = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        viewCont.present(alertCont, animated: true, completion: nil)
    }
}
