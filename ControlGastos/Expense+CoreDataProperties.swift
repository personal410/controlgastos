//
//  Expense+CoreDataProperties.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Expense{
    @NSManaged var amount:Float
    @NSManaged var comment:String
    @NSManaged var expenseId:Int32
    @NSManaged var day:Day
    @NSManaged var subcategory:Subcategory
}