//
//  SubcategoryTableViewCell.swift
//  ControlGastos
//
//  Created by Victor Salazar on 20/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import UIKit
class SubcategoryTableViewCell:UITableViewCell{
    @IBOutlet weak var subcategoryNameLbl:UILabel!
    @IBOutlet weak var defaultAmountLbl:UILabel!
}