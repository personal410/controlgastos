//
//  SubcategoriesTableViewController.swift
//  ControlGastos
//
//  Created by Victor Salazar on 20/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import UIKit
class SubcategoriesTableViewController:UITableViewController{
    //MARK: - Variables
    weak var category:Category!
    var currentTxtFld:UITextField?
    var arrSubcategories:Array<Subcategory> = []
    var currentSubcategory:Subcategory?
    lazy var newSubcategoryAlertCont:UIAlertController = {
        let alertCont = UIAlertController(title: "Subcategoria", message: nil, preferredStyle: .alert)
        alertCont.addTextField{(subcategoryNameTxtFld:UITextField) -> Void in
            subcategoryNameTxtFld.placeholder = "Nueva Subcategoria"
            subcategoryNameTxtFld.addTarget(self, action: #selector(self.txtFldDidChangeEditing), for: .editingChanged)
        }
        alertCont.addTextField{(defaultAmountTxtFld:UITextField) -> Void in
            defaultAmountTxtFld.placeholder = "Monto por defecto"
            defaultAmountTxtFld.keyboardType = .decimalPad
            defaultAmountTxtFld.addTarget(self, action: #selector(self.txtFldDidChangeEditing), for: .editingChanged)
        }
        alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        let saveAction = UIAlertAction(title: "Guardar", style: .default, handler:{(action:UIAlertAction) -> Void in
            let txtFlds = self.newSubcategoryAlertCont.textFields!
            let subcatNameTxtFld = txtFlds.first!
            let arrSubcategoriesFilter = self.arrSubcategories.filter {$0.subcategoryName == subcatNameTxtFld.text!}
            if arrSubcategoriesFilter.count == 0 {
                let defAmountTxtFld = txtFlds[1]
                let defAmount = (defAmountTxtFld.text! as NSString).floatValue
                Subcategory.createSubcategory(subcategoryName: subcatNameTxtFld.text!, defaultAmount: defAmount, category: self.category)
                self.arrSubcategories = self.category!.subcategories!.array as! Array<Subcategory>
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: [IndexPath(row: self.arrSubcategories.count - 1, section: 0)], with: .top)
                self.tableView.endUpdates()
            }else{
            
            }
        })
        saveAction.isEnabled = false
        alertCont.addAction(saveAction)
        return alertCont
    }()
    //MARK: - ViewController
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        self.navigationItem.title = category!.categoryName
        arrSubcategories = category!.subcategories!.array as! Array<Subcategory>
    }
    override func viewWillDisappear(_ animated:Bool){
        super.viewWillDisappear(animated)
        currentTxtFld?.resignFirstResponder()
        NotificationCenter.default.removeObserver(self)
    }
    //MARK: - IBAction
    @IBAction func addNewSubcategory(){
        self.present(newSubcategoryAlertCont, animated: true, completion: nil)
    }
    //MARK: - TextField
    @objc func txtFldDidChangeEditing(){
        let txtFlds = self.newSubcategoryAlertCont.textFields!
        let subcatNameTxtFld = txtFlds.first!
        let defAmountTxtFld = txtFlds[1]
        self.newSubcategoryAlertCont.actions[1].isEnabled = !subcatNameTxtFld.text!.isEmpty && !defAmountTxtFld.text!.isEmpty
    }
    //MARK: - TableView
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrSubcategories.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subcategoryCell", for: indexPath) as! SubcategoryTableViewCell
        let subcategoryTemp = arrSubcategories[indexPath.row]
        cell.subcategoryNameLbl.text = subcategoryTemp.subcategoryName
        cell.defaultAmountLbl.text = String(format: "%.2f", arguments: [subcategoryTemp.defaultAmount])
        return cell
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Eliminar"
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let subcategoryTemp = arrSubcategories[indexPath.row]
            Subcategory.deleteSubcategory(subcategory: subcategoryTemp)
            arrSubcategories.remove(at: indexPath.row)
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [indexPath], with: .top)
            self.tableView.endUpdates()
        }
    }
    //MARK: - Keyboard
    @objc func showKeyboard(notification:NSNotification){
        let info = notification.userInfo!
        let keyboardFrameValue = info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        let keyboardHeight = keyboardFrame.height
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
    }
    @objc func hideKeyboard(_:NSNotification){
        let newEdgeInset = UIEdgeInsets.zero
        self.tableView.contentInset = newEdgeInset
        self.tableView.scrollIndicatorInsets = newEdgeInset
    }
}
