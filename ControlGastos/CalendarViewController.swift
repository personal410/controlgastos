//
//  CalendarViewController.swift
//  ControlGastos
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import UIKit
class CalendarViewController:UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    //MARK: - Variables
    var daySize:CGFloat = 0
    var arrInfoMonths:Array<MonthYear> = []
    var arrMonths:Array<(monthYear:MonthYear, monthView:UIView)> = []
    var currentDay:Int = -1
    var didChangeCurrentDay = false
    var newExpense:Expense? = nil
    var deletedExpense:Expense? = nil
    var arrDicCategories:Array<Dictionary<String, AnyObject>> = []
    //MARK: - IBOutlet
    @IBOutlet weak var leftBtn:UIButton!
    @IBOutlet weak var rightBtn:UIButton!
    @IBOutlet weak var monthLbl:UILabel!
    @IBOutlet weak var expensesTableView:UITableView!
    @IBOutlet weak var calendarsScrollView:UIScrollView!
    @IBOutlet weak var calScrollViewHeightConst:NSLayoutConstraint!
    //MARK: - ViewController
    override func viewDidLoad(){
        super.viewDidLoad()
        self.rightBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        daySize = (self.view.frame.width - 16) / 7
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        for view in self.calendarsScrollView.subviews {
            view.removeFromSuperview()
        }
        let currentMonth = ToolBox.normalizeDate(date: NSDate())
        let previousMonth = ToolBox.getMonthWithDate(date: currentMonth as NSDate, withDirection: -1)
        let nextMonth = ToolBox.getMonthWithDate(date: currentMonth as NSDate, withDirection: 1)
        
        let previousMonthYearView = self.createMonthYearViewWithDate(date: previousMonth, withIndex: 0)
        let currentMonthYearView = self.createMonthYearViewWithDate(date: currentMonth as NSDate, withIndex: 1)
        let nextMonthYearView = self.createMonthYearViewWithDate(date: nextMonth, withIndex: 2)
        arrMonths = [previousMonthYearView, currentMonthYearView, nextMonthYearView]
        
        self.calendarsScrollView.contentSize = CGSize(width: self.view.frame.width * 3, height: currentMonthYearView.monthView.frame.height)
        self.calScrollViewHeightConst.constant = currentMonthYearView.monthView.frame.height
        self.calendarsScrollView.contentOffset = CGPoint(x: self.view.frame.width, y: 0)
        
        self.calendarsScrollView.addSubview(previousMonthYearView.monthView)
        self.calendarsScrollView.addSubview(currentMonthYearView.monthView)
        self.calendarsScrollView.addSubview(nextMonthYearView.monthView)
        self.setCurrentMonthTitle()
        if currentDay == -1 {
            currentDay = ToolBox.getCalendar().component(.day, from: NSDate() as Date) + currentMonthYearView.monthYear.firstDay - 1
        }
        let ctrlTemp = UIControl()
        ctrlTemp.tag = currentDay
        self.didSelectDay(ctrlTemp)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewCont = segue.destination as? SubcategoriesExpensesTableViewController {
            let dicCategory = arrDicCategories[self.expensesTableView.indexPathForSelectedRow!.row]
            let arrExpenses = dicCategory["arrExpenses"] as! Array<Expense>
            viewCont.arrExpenses = arrExpenses
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrDicCategories.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath)
        let dicCategory = arrDicCategories[indexPath.row]
        cell.textLabel?.text = (dicCategory["category"] as! Category).categoryName
        cell.detailTextLabel?.text = String(format: "%.2f", (dicCategory["totalAmount"] as! Float))
        return cell
    }
    //MARK: - ScrollView
    func scrollViewDidEndDecelerating(_ scrollView:UIScrollView){
        if scrollView == self.calendarsScrollView {
            let contentOffsetX = scrollView.contentOffset.x
            var didMove = contentOffsetX == 0 || contentOffsetX == CGFloat(2) * self.view.frame.width
            if didMove && !didChangeCurrentDay {
                let ctrl = self.arrMonths[1].monthView.viewWithTag(currentDay) as! UIControl
                ctrl.backgroundColor = UIColor.clear
                currentDay = self.arrMonths[Int(contentOffsetX / self.view.frame.width)].monthYear.firstDay
            }
            if contentOffsetX == 0 {
                let nextMonthYearView = self.arrMonths[2]
                nextMonthYearView.monthView.removeFromSuperview()
                arrMonths.remove(at: 2)
                let prevMonthYearView = self.arrMonths[0]
                let currentDate = prevMonthYearView.monthYear.dateTemp()
                let prevDate = ToolBox.getMonthWithDate(date: currentDate, withDirection: -1)
                for monthYearView in arrMonths {
                    var frameTemp = monthYearView.monthView.frame
                    frameTemp.origin.x = frameTemp.origin.x + self.view.frame.width
                    monthYearView.monthView.frame = frameTemp
                }
                let newPrevMonthYearView = self.createMonthYearViewWithDate(date: prevDate, withIndex: 0)
                arrMonths.insert(newPrevMonthYearView, at: 0)
                self.calendarsScrollView.addSubview(newPrevMonthYearView.monthView)
                self.setCurrentMonthTitle()
                self.calendarsScrollView.contentOffset = CGPoint(x: self.view.frame.width, y: 0)
                self.calendarsScrollView.contentSize = CGSize(width: self.view.frame.width * 3, height: self.daySize * CGFloat(prevMonthYearView.monthYear.numberLines))
                self.calScrollViewHeightConst.constant = self.daySize * CGFloat(prevMonthYearView.monthYear.numberLines)
                didMove = true
            }else if contentOffsetX == self.view.frame.width * CGFloat(2) {
                let prevMonthYearView = self.arrMonths[0]
                prevMonthYearView.monthView.removeFromSuperview()
                arrMonths.removeFirst()
                let nextMonthYearView = self.arrMonths[1]
                let currentDate = nextMonthYearView.monthYear.dateTemp()
                let nextDate = ToolBox.getMonthWithDate(date: currentDate, withDirection: 1)
                for monthYearView in arrMonths {
                    var frameTemp = monthYearView.monthView.frame
                    frameTemp.origin.x = frameTemp.origin.x - self.view.frame.width
                    monthYearView.monthView.frame = frameTemp
                }
                let newNextMonthYearView = self.createMonthYearViewWithDate(date: nextDate, withIndex: 2)
                arrMonths.append(newNextMonthYearView)
                self.calendarsScrollView.addSubview(newNextMonthYearView.monthView)
                self.setCurrentMonthTitle()
                self.calendarsScrollView.contentOffset = CGPoint(x: self.view.frame.width, y: 0)
                self.calendarsScrollView.contentSize = CGSize(width: self.view.frame.width * 3, height: self.daySize * CGFloat(nextMonthYearView.monthYear.numberLines))
                self.calScrollViewHeightConst.constant = self.daySize * CGFloat(nextMonthYearView.monthYear.numberLines)
                didMove = true
            }
            if didMove && !didChangeCurrentDay {
                selectDay()
            }
            if didChangeCurrentDay {
                didChangeCurrentDay = false
            }
        }
    }
    //MARK: - IBAction
    @IBAction func moveCurrentMonth(_ sender:UIButton){
        let nextOrPreviousMonthYear = self.arrMonths[sender.tag].monthYear
        let ctrl = self.arrMonths[1].monthView.viewWithTag(currentDay) as! UIControl
        ctrl.backgroundColor = UIColor.clear
        currentDay = nextOrPreviousMonthYear.firstDay
        didChangeCurrentDay = true
        UIView.animate(withDuration: 0.5, animations:{() in
            self.calendarsScrollView.contentOffset = CGPoint(x: self.view.frame.width * CGFloat(sender.tag), y: 0)
        }){(b:Bool) in
            self.scrollViewDidEndDecelerating(self.calendarsScrollView)
            self.selectDay()
        }
    }
    @IBAction func createNewExpense(){
        let arrCategories = Category.getAllCategories()
        var canCreateExpense = false
        if arrCategories.count > 0 {
            for category in arrCategories {
                if category.subcategories!.count > 0 {
                    canCreateExpense = true
                    break
                }
            }
        }
        if canCreateExpense {
            self.performSegue(withIdentifier: "showNewExpense", sender: nil)
        }else{
            ToolBox.showAlertWithTitle(title: "Alerta", withMessage: "No ha creado categories o subcategorias", inViewCont: self)
        }
    }
    //MARK: - Auxiliar
    func createMonthYearViewWithDate(date:NSDate, withIndex index:Int) -> (monthYear:MonthYear, monthView:UIView) {
        let currentMonthYear:MonthYear = self.getMonthYearWithDate(date: date)
        var previousMonthYear:MonthYear? = nil
        if currentMonthYear.firstDay > 1 {
            let previousDate = ToolBox.getMonthWithDate(date: date, withDirection: -1)
            previousMonthYear = self.getMonthYearWithDate(date: previousDate)
        }
        let monthView = UIView(frame: CGRect(x: CGFloat(index) * self.view.frame.width + 8, y: 0, width: self.view.frame.width, height: daySize * CGFloat(currentMonthYear.numberLines)))
        for i in 0 ..< (currentMonthYear.numberLines * 7) {
            let ind = i + 1
            let dayCtrl = UIControl(frame: CGRect(x: CGFloat(i%7) * daySize, y: floor(CGFloat(i) / 7.0) * daySize, width: daySize, height: daySize))
            dayCtrl.tag = ind
            dayCtrl.addTarget(self, action: #selector(didSelectDay(_:)), for: .touchUpInside)
            let dayLbl = UILabel(frame: CGRect(x: 0, y: 0, width: daySize, height: daySize))
            dayLbl.textAlignment = .center
            dayLbl.numberOfLines = 2
            dayLbl.backgroundColor = UIColor.clear
            var dayNumber = 0
            var amount:CGFloat = 0
            var arrDays:[Day] = []
            if ind < currentMonthYear.firstDay {
                dayNumber = previousMonthYear!.numberDays + 1 - currentMonthYear.firstDay + ind
                let dateComps = NSDateComponents()
                dateComps.day = dayNumber
                dateComps.month = previousMonthYear!.month
                dateComps.year = previousMonthYear!.year
                let date = ToolBox.getCalendar().date(from: dateComps as DateComponents)
                arrDays = Day.getAllDaysWithDate(date: date! as NSDate)
                dayLbl.textColor = UIColor.lightGray
            }else if ind < currentMonthYear.firstDay + currentMonthYear.numberDays {
                dayNumber = ind + 1 - currentMonthYear.firstDay
                dayLbl.textColor = UIColor.black
                let todayDayComps = ToolBox.getCalendar().components([.day, .month, .year], from: NSDate() as Date)
                if todayDayComps.month == currentMonthYear.month && todayDayComps.year == currentMonthYear.year {
                    if todayDayComps.day == dayNumber {
                        dayLbl.textColor = UIColor.red
                    }
                }
                let dateComps = NSDateComponents()
                dateComps.day = dayNumber
                dateComps.month = currentMonthYear.month
                dateComps.year = currentMonthYear.year
                let date = ToolBox.getCalendar().date(from: dateComps as DateComponents)
                arrDays = Day.getAllDaysWithDate(date: date as NSDate?)
            }else{
                dayNumber = ind + 1 - currentMonthYear.firstDay - currentMonthYear.numberDays
                dayLbl.textColor = UIColor.lightGray
            }
            let finalAttributeText = NSMutableAttributedString(string: "")
            finalAttributeText.append(NSAttributedString(string: "\(dayNumber)\n", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]))
            if arrDays.count > 0 {
                let day = arrDays.first!
                amount = CGFloat(day.totalAmount)
                if amount > 0 {
                    finalAttributeText.append(NSAttributedString(string: String(format: "S/. %.2f", arguments: [amount]), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 8)]))
                }else{
                    finalAttributeText.append(NSAttributedString(string: " ", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 8)]))
                }
            }else{
                finalAttributeText.append(NSAttributedString(string: " ", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 8)]))
            }
            dayLbl.attributedText = finalAttributeText
            dayCtrl.addSubview(dayLbl)
            monthView.addSubview(dayCtrl)
        }
        return (currentMonthYear, monthView)
    }
    func getMonthYearWithDate(date:NSDate) -> MonthYear {
        let monthYear:MonthYear
        let arrInfoMonthsTemp = arrInfoMonths.filter(){$0.dateTemp() == date}
        if arrInfoMonthsTemp.count > 0 {
            monthYear = arrInfoMonthsTemp.first!
        }else{
            monthYear = MonthYear(date: date)
            arrInfoMonths.append(monthYear)
        }
        return monthYear
    }
    func setCurrentMonthTitle(){
        let currentMonthYearView = self.arrMonths[1]
        let currentMonth = currentMonthYearView.monthYear.dateTemp()
        self.monthLbl.text = ToolBox.getDate(date: currentMonth, withFormat: "MMMM yyyy").capitalized
    }
    @objc func didSelectDay(_ ctrl:UIControl){
        let ind = ctrl.tag
        if currentDay > -1 {
            let ctrl = self.arrMonths[1].monthView.viewWithTag(currentDay) as! UIControl
            ctrl.backgroundColor = UIColor.clear
        }
        currentDay = ind
        let currentMonthYear = arrMonths[1].monthYear
        if ind < currentMonthYear.firstDay {
            let previousMonthYear = self.arrMonths[0].monthYear
            currentDay = (previousMonthYear.numberLines - 1) * 7 + currentDay
            didChangeCurrentDay = true
            UIView.animate(withDuration: 0.5, animations:{() in
                self.calendarsScrollView.contentOffset = CGPoint(x: 0, y: 0)
                }){(b:Bool) in
                    self.scrollViewDidEndDecelerating(self.calendarsScrollView)
                    self.selectDay()
            }
        }else if ind < currentMonthYear.firstDay + currentMonthYear.numberDays {
            selectDay()
        }else{
            let currentMonthYear = self.arrMonths[1].monthYear
            currentDay = currentDay - (currentMonthYear.numberLines - 1) * 7
            didChangeCurrentDay = true
            UIView.animate(withDuration: 0.5, animations:{() in
                self.calendarsScrollView.contentOffset = CGPoint(x: CGFloat(2) * self.view.frame.width, y: 0)
                }){(b:Bool) in
                    self.scrollViewDidEndDecelerating(self.calendarsScrollView)
                    self.selectDay()
            }
        }
    }
    func selectDay(){
        let ctrl = self.arrMonths[1].monthView.viewWithTag(currentDay) as! UIControl
        ctrl.backgroundColor = UIColor(red: 220/255.0, green: 220/255.0, blue: 220/255.0, alpha: 1.0)
        let currentMonthYear = self.arrMonths[1].monthYear
        let day = currentDay - currentMonthYear.firstDay + 1
        let dateComps = NSDateComponents()
        dateComps.day = day
        dateComps.month = currentMonthYear.month
        dateComps.year = currentMonthYear.year
        let currentDate = ToolBox.getCalendar().date(from: dateComps as DateComponents)!
        let arrDays = Day.getAllDaysWithDate(date: currentDate as NSDate?)
        arrDicCategories = []
        if arrDays.count > 0 {
            let selectedDay = arrDays.first!
            let arrExpenses = selectedDay.expenses!.array as! [Expense]
            for category in Category.getAllCategories() {
                let arrExpensesFilter = arrExpenses.filter(){$0.subcategory.category.categoryName == category.categoryName}
                if arrExpensesFilter.count > 0 {
                    let totalAmount = arrExpensesFilter.map(){$0.amount}.reduce(0, +)
                    arrDicCategories.append(["category": category, "totalAmount": totalAmount as AnyObject, "arrExpenses": arrExpensesFilter as AnyObject])
                }
            }
        }
        self.expensesTableView.reloadData()
    }
}
