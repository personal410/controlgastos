//
//  NewExpenseViewController.swift
//  ControlGastos
//
//  Created by Victor Salazar on 23/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import UIKit
class NewExpenseViewController:UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //MARK: - Variables
    var arrCategories:Array<Category> = Category.getAllCategories().filter(){$0.subcategories!.count > 0}
    var currentCategory:Category? {
        didSet{
            self.categoryTxtFld.text = currentCategory?.categoryName
        }
    }
    var currentSubcategory:Subcategory? {
        didSet{
            self.subcategoryTxtFld.text = currentSubcategory?.subcategoryName
            self.amountTxtFld.text = String(format: "%.1f", arguments: [currentSubcategory!.defaultAmount])
        }
    }
    var expenseDate = NSDate()
    var expense:Expense?
    var lockedCategory:Category?
    var lockedDate:NSDate?
    //MARK: - IBOutlets
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var categoryTxtFld:UITextField!
    @IBOutlet weak var subcategoryTxtFld:UITextField!
    @IBOutlet weak var amountTxtFld:UITextField!
    @IBOutlet weak var dateTxtFld:UITextField!
    @IBOutlet weak var commentTxtView:UITextView!
    //MARK: - ViewController
    override func viewDidLoad(){
        super.viewDidLoad()
        let dateComps = ToolBox.getCalendar().components([.day, .month, .year], from: expenseDate as Date)
        expenseDate = ToolBox.getCalendar().date(from: dateComps)! as NSDate
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.commentTxtView.layer.borderColor = UIColor(red: 220/255.0, green: 220/255.0, blue: 220/255.0, alpha: 1.0).cgColor
        self.commentTxtView.layer.cornerRadius = 8.0
        if expense != nil {
            currentCategory = self.expense!.subcategory.category
            currentSubcategory = self.expense!.subcategory
            expenseDate = self.expense!.day.date
            self.categoryTxtFld.isEnabled = false
            self.subcategoryTxtFld.isEnabled = false
            self.dateTxtFld.isEnabled = false
            self.commentTxtView.text = self.expense!.comment
            self.amountTxtFld.text = String(format:"%.1f", arguments: [self.expense!.amount])
        }else{
            if lockedCategory == nil {
                currentCategory = arrCategories.first!
            }else{
                currentCategory = lockedCategory
                self.categoryTxtFld.isEnabled = false
            }
            currentSubcategory = currentCategory!.subcategories?.firstObject as? Subcategory
            if lockedDate == nil {
                let datePicker = UIDatePicker()
                datePicker.datePickerMode = .date
                datePicker.date = expenseDate as Date
                datePicker.maximumDate = expenseDate as Date
                datePicker.addTarget(self, action: #selector(datePickerDidChangeValue(datePicker:)), for: .valueChanged)
                self.dateTxtFld.inputView = datePicker
            }else{
                self.dateTxtFld.isEnabled = false
                expenseDate = lockedDate!
            }
            self.addPickerToTextField(textField: self.categoryTxtFld)
            self.addPickerToTextField(textField: self.subcategoryTxtFld)
        }
        self.dateTxtFld.text = ToolBox.getDate(date: expenseDate, withFormat: "dd/MM/yyyy")
    }
    override func viewWillDisappear(_ animated:Bool){
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    //MARK: - PickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView:UIPickerView, numberOfRowsInComponent component:Int) -> Int {
        if pickerView.tag == 0 {
            return arrCategories.count
        }else{
            return currentCategory!.subcategories!.count
        }
    }
    func pickerView(_ pickerView:UIPickerView, titleForRow row:Int, forComponent component:Int) -> String? {
        if pickerView.tag == 0 {
            return arrCategories[row].categoryName
        }else{
            return (currentCategory!.subcategories!.object(at: row) as! Subcategory).subcategoryName
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            currentCategory = arrCategories[row]
            self.categoryTxtFld.text = currentCategory?.categoryName
            currentSubcategory = currentCategory!.subcategories?.firstObject as? Subcategory
        }else{
            currentSubcategory = currentCategory!.subcategories!.object(at: row) as? Subcategory
        }
    }
    //MARK: - IBAction
    @IBAction func dismissKeyboard(){
        categoryTxtFld.resignFirstResponder()
        subcategoryTxtFld.resignFirstResponder()
        amountTxtFld.resignFirstResponder()
        dateTxtFld.resignFirstResponder()
        commentTxtView.resignFirstResponder()
    }
    @IBAction func saveExpense(){
        self.dismissKeyboard()
        if self.amountTxtFld.text!.count == 0 {
            ToolBox.showAlertWithTitle(title: "Alerta", withMessage: "Asigne el monto del gasto", inViewCont: self)
        }else{
            let amount = (self.amountTxtFld.text! as NSString).floatValue
            let comment = self.commentTxtView.text
            if expense == nil {
                let newExpense = Expense.createExpense(subcategory: currentSubcategory!, amount: amount, date: expenseDate, comment: comment!)
                if lockedCategory != nil {
                    let viewCont = self.navigationController!.viewControllers[1] as! SubcategoriesExpensesTableViewController
                    viewCont.newExpense = newExpense
                }
            }else{
                if self.expense!.amount != amount {
                    let diff = amount - self.expense!.amount
                    self.expense!.day.totalAmount = self.expense!.day.totalAmount + Double(diff)
                }
                self.expense!.amount = amount
                self.expense!.comment = comment!
                AppDelegate.saveContext()
                let viewCont = self.navigationController!.viewControllers[1] as! SubcategoriesExpensesTableViewController
                viewCont.expenseModified = self.expense
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    //MARK: - Auxiliar
    @objc func showKeyboard(notification:NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let newEdgeInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        self.scrollView.contentInset = newEdgeInset
        self.scrollView.scrollIndicatorInsets = newEdgeInset
    }
    @objc func hideKeyboard(notification:NSNotification){
        let newEdgeInset = UIEdgeInsets.zero
        self.scrollView.contentInset = newEdgeInset
        self.scrollView.scrollIndicatorInsets = newEdgeInset
    }
    func addPickerToTextField(textField:UITextField){
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.tag = textField.tag
        textField.inputView = pickerView
    }
    @objc func datePickerDidChangeValue(datePicker:UIDatePicker){
        self.expenseDate = datePicker.date as NSDate
        self.dateTxtFld.text = ToolBox.getDate(date: expenseDate, withFormat: "dd/MM/yyyy")
    }
}
